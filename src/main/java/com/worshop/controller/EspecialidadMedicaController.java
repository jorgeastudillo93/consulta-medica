package com.worshop.controller;

import java.awt.PageAttributes.MediaType;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.worshop.models.EspecialidadMedica;
import com.worshop.service.IEspecialidadMedicaService;

@RestController
@RequestMapping("/especialidadesmedicas")
public class EspecialidadMedicaController {
	
	@Autowired
	IEspecialidadMedicaService service;
	@GetMapping(produces=MediaType.APLLICATION_JSON_VALUE)
	public ResponseEntity<EspecialidadMedica> listarId(@RequestBody Integer id){
		List<EspecialidadMedica> especialidad=new ArrayList<>();
		EspecialidadMedica especialista=service.findById(id);
		return new ResponseEntity<EspecialidadMedica>(especialista, HttpStatus.OK);
	}
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public EspecialidadMedica registrar(@RequestBody EspecialidadMedica v) {
		return service.persist(v);
	}
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EspecialidadMedica> listarId(@PatchVariable("id") Integer id){
		EspecialidadMedica especialista=service.findById(id);
		return new ResponseEntity<EspecialidadMedica>(especialista, HttpStatus.OK);
	}
	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object>actualizar(@Valid @ResquestBody EspecialidadMedica especialidad){
		service.merge(especialidad);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
		EspecialidadMedica espec=service.findById(id);
		/*if(espec==null) {
			throw new ModeloNotFoundException("ID:"+id);
		}
		else
		{
			service.delete(id);	
		}*/
	}
}
