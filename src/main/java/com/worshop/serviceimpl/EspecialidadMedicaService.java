package com.worshop.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worshop.dao.IEspecilidadMedicaDAO;
import com.worshop.models.EspecialidadMedica;
import com.worshop.service.IEspecialidadMedicaService;
@Service
public class EspecialidadMedicaService implements IEspecialidadMedicaService{
	
	@Autowired
	IEspecilidadMedicaDAO service;
	@Override
	public EspecialidadMedica persist(EspecialidadMedica e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public List<EspecialidadMedica> getALL() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public EspecialidadMedica findById(Integer id) {
		// TODO Auto-generated method stub
		return service.findOne(id);
	}

	/* (non-Javadoc)
	 * @see com.worshop.service.IEspecialidadMedicaService#merge(com.worshop.models.EspecialidadMedica)
	 */
	@Override
	public EspecialidadMedica merge(EspecialidadMedica e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		service.delete(id);
	}

}
